import {useContext, useEffect, useState} from "react";

import { Button, Table, Modal, Form } from "react-bootstrap";
import {Navigate} from "react-router-dom";
import Swal from "sweetalert2";

import UserContext from "../UserContext";

export default function AdminDashboard(){

	// const { user } = useContext(UserContext);
	const [userRole, setUserRole] = useState(localStorage.getItem("userRole"));

	
	const [allProducts, setAllProducts] = useState([]);
	const [allUsers, setAllUsers] = useState([]);


	const [productId, setProductId] = useState("");
	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);
	const [category, setCategory] = useState("")
	const [stocks, setStocks] = useState(0);

  
    const [isActive, setIsActive] = useState(false);

    // State for Add/Edit Modal
    const [showAdd, setShowAdd] = useState(false);
	const [showEdit, setShowEdit] = useState(false);


	const openAdd = () => setShowAdd(true); //Will show the modal
	const closeAdd = () => setShowAdd(false); //Will hide the modal


	const openEdit = (id) => {
		setProductId(id);

		
		fetch(`${ process.env.REACT_APP_API_URL }/products/${id}`)
		.then(res => res.json())
		.then(data => {

			console.log(data);

			
			setName(data.name);
			setDescription(data.description);
			setPrice(data.price);
			setCategory(data.category);
			setStocks(data.stocks);
		});

		setShowEdit(true)
	};

	const closeEdit = () => {

		// Clear input fields upon closing the modal
	    setName('');
	    setDescription('');
	    setPrice(0);
			setCategory('');
	    setStocks(0);

		setShowEdit(false);
	};


	const fetchData = () =>{
		// get all the products from the database
		fetch(`${process.env.REACT_APP_API_URL}/products/all`, {
			headers:{
				"Authorization": `Bearer ${localStorage.getItem("token")}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			setAllProducts(data.map(product => {
				return (
					<tr key={product._id}>
						<td>{product._id}</td>
						<td>{product.name}</td>
						<td>{product.description}</td>
						<td>{product.price}</td>
						<td>{product.stocks}</td>
						<td>{product.isActive ? "Active" : "Inactive"}</td>
						<td>
							{
								// conditonal rendering on what button should be visible base on the status of the product
								(product.isActive)
								?
									<Button variant="danger" size="sm" onClick={() => archive(product._id, product.name)}>Archive</Button>
								:
									<>
										<Button variant="success" size="sm" className="mx-1" onClick={() => unarchive(product._id, product.name)}>Unarchive</Button>
										<Button variant="secondary" size="sm" className="mx-1" onClick={() => openEdit(product._id)}>Edit</Button>
									</>

							}
						</td>
					</tr>
				)
			}));
		});
	}
		
 

	// to fetch all products in the first render of the page.
	useEffect(()=>{
		fetchData();
	}, [])

	const fetchUserData = () =>{
		// get all the users from the database
		fetch(`${process.env.REACT_APP_API_URL}/users/all`, {
			headers:{
				"Authorization": `Bearer ${localStorage.getItem("token")}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			setAllUsers(data.map(user => {
				return (
					<tr key={user._id}>
						<td>{user._id}</td>
						<td>{user.firstName}</td>
						<td>{user.email}</td>
						<td>{user.mobileNumber}</td>
						<td>{user.isAdmin ? "Admin" : "notAdmin"}</td>
						<td>
							{
								// conditonal rendering on what button should be visible base on the status of the users
								(user.isAdmin)
								?
									<Button variant="danger" disabled size="sm" >isAdmin</Button>
								:
									<>
										<Button variant="success" size="sm" className="mx-1" onClick={() => promote(user._id, user.firstName)}>Promote</Button>
										
									</>

							}
						</td>
					</tr>
				)
			}));
		});
	}

	useEffect(()=>{
		fetchUserData();
	}, [])

	const promote = (id, userEmail) =>{
		console.log(id);
		console.log(userEmail);


		fetch(`${process.env.REACT_APP_API_URL}/users/${id}/toAdmin`, {
			method: "PATCH",
			headers:{
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem("token")}`
			},
			body: JSON.stringify({
				isAdmin: true
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			if(data){
				Swal.fire({
					title: "promoteSuccessful",
					icon: "success",
					text: `${userEmail} is now an Admin.`
				});
				// To show the update with the specific operation intiated.
				fetchData();
			}
			else{
				Swal.fire({
					title: "promote unsuccessful",
					icon: "error",
					text: "Something went wrong. Please try again later!"
				});
			}
		})
	}

	// [SECTION] Setting the product to Active/Inactive

	// Making the product inactive
	const archive = (id, productName) =>{
		console.log(id);
		console.log(productName);

		// Using the fetch method to set the isActive property of the product document to false
		fetch(`${process.env.REACT_APP_API_URL}/products/${id}/archive`, {
			method: "PUT",
			headers:{
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem("token")}`
			},
			body: JSON.stringify({
				isActive: false
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			if(data){
				Swal.fire({
					title: "Archive Successful",
					icon: "success",
					text: `${productName} is now inactive.`
				});
				// To show the update with the specific operation intiated.
				fetchData();
			}
			else{
				Swal.fire({
					title: "Archive unsuccessful",
					icon: "error",
					text: "Something went wrong. Please try again later!"
				});
			}
		})
	}

	// Making the product active
	const unarchive = (id, productName) =>{
		console.log(id);
		console.log(productName);

		// Using the fetch method to set the isActive property of the product document to false
		fetch(`${process.env.REACT_APP_API_URL}/products/${id}/archive`, {
			method: "PUT",
			headers:{
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem("token")}`
			},
			body: JSON.stringify({
				isActive: true
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			if(data){
				Swal.fire({
					title: "Unarchive Successful",
					icon: "success",
					text: `${productName} is now active.`
				});
				// To show the update with the specific operation intiated.
				fetchData();
			}
			else{
				Swal.fire({
					title: "Unarchive Unsuccessful",
					icon: "error",
					text: "Something went wrong. Please try again later!"
				});
			}
		})
	}

	// [SECTION] Adding a new product
	// Inserting a new product in our database
	const addProduct = (e) =>{
			// Prevents page redirection via form submission
		    e.preventDefault();

		    fetch(`${process.env.REACT_APP_API_URL}/products/create`, {
		    	method: "POST",
		    	headers: {
					"Content-Type": "application/json",
					"Authorization": `Bearer ${localStorage.getItem('token')}`
				},
				body: JSON.stringify({
				    name: name,
				    description: description,
				    price: price,
						category: category,
				    stocks: stocks
				})
		    })
		    .then(res => res.json())
		    .then(data => {
		    	console.log(data);

		    	if(data){
		    		Swal.fire({
		    		    title: "Product succesfully Added",
		    		    icon: "success",
		    		    text: `${name} is now added`
		    		});

		    		// To automatically add the update in the page
		    		fetchData();
		    		// Automatically closed the modal
		    		closeAdd();
		    	}
		    	else{
		    		Swal.fire({
		    		    title: "Error!",
		    		    icon: "error",
		    		    text: `Something went wrong. Please try again later!`
		    		});
		    		closeAdd();
		    	}

		    })

		    // Clear input fields
		    setName('');
		    setDescription('');
		    setPrice(0);
				setCategory('');
		    setStocks(0);
	}

	// [SECTION] Edit a specific product
	// Updating a specific product in our database
	// edit a specific product
	const editProduct = (e) =>{
			// Prevents page redirection via form submission
		    e.preventDefault();

		    fetch(`${process.env.REACT_APP_API_URL}/products/${productId}/update`, {
		    	method: "PUT",
		    	headers: {
					"Content-Type": "application/json",
					"Authorization": `Bearer ${localStorage.getItem('token')}`
				},
				body: JSON.stringify({
				    name: name,
				    description: description,
				    price: price,
						category: category,
				    stocks: stocks
				})
		    })
		    .then(res => res.json())
		    .then(data => {
		    	console.log(data);

		    	if(data){
		    		Swal.fire({
		    		    title: "Product succesfully Updated",
		    		    icon: "success",
		    		    text: `${name} is now updated`
		    		});

		    		// To automatically add the update in the page
		    		fetchData();
		    		// Automatically closed the form
		    		closeEdit();

		    	}
		    	else{
		    		Swal.fire({
		    		    title: "Error!",
		    		    icon: "error",
		    		    text: `Something went wrong. Please try again later!`
		    		});

		    		closeEdit();
		    	}

		    })

		    // Clear input fields
		    setName('');
		    setDescription('');
		    setPrice(0);
		    setStocks(0);
	} 

	// Submit button validation for add/edit product
	useEffect(() => {

        // Validation to enable submit button when all fields are populated and set a price and stock greater than zero.
        if(name !== "" && description !== "" && price > 0 && stocks > 0){
            setIsActive(true);
        } else {
            setIsActive(false);
        }

    }, [name, description, price, stocks]);

	return(
		(userRole)
		?
		<>
			{/*Header for the admin dashboard and functionality for create product and show */}
			<div className="mt-5 mb-3 text-center">
				<h1>Admin Dashboard</h1>
				{/*Adding a new product */}
				<Button variant="success" className="
				mx-2" onClick={openAdd}>Add Product</Button>
	
				
			</div>
			{/*End of admin dashboard header*/}

			{/*For view all the products in the database.*/}
			<Table striped bordered hover>
		      <thead>
		        <tr>
		          <th>Product ID</th>
		          <th>Product Name</th>
		          <th>Description</th>
		          <th>Price</th>
		          <th>Stocks</th>
		          <th>Status</th>
		          <th>Actions</th>
		        </tr>
		      </thead>
		      <tbody>
	        	{allProducts}
		      </tbody>
		    </Table>
			{/*End of table for product viewing*/}
			<Table striped bordered hover>
  <thead>
    <tr>
      <th>User ID</th>
      <th> first Name</th>
      <th>Email</th>
      <th>Mobile Number</th>
      <th>Status</th>
			<th>Actions</th>
    </tr>
  </thead>
  <tbody>
    {allUsers}
  </tbody>
</Table>

			

	    	{/*Modal for Adding a new product*/}
	        <Modal show={showAdd} fullscreen={true} onHide={closeAdd}>
	    		<Form onSubmit={e => addProduct(e)}>

	    			<Modal.Header closeButton>
	    				<Modal.Title>Add New Product</Modal.Title>
	    			</Modal.Header>

	    			<Modal.Body>
	    	        	<Form.Group controlId="name" className="mb-3">
	    	                <Form.Label>Product Name</Form.Label>
	    	                <Form.Control 
	    		                type="text" 
	    		                placeholder="Enter Product Name" 
	    		                value = {name}
	    		                onChange={e => setName(e.target.value)}
	    		                required
	    	                />
	    	            </Form.Group>

	    	            <Form.Group controlId="description" className="mb-3">
	    	                <Form.Label>Product Description</Form.Label>
	    	                <Form.Control
	    	                	as="textarea"
	    	                	rows={3}
	    		                placeholder="Enter Product Description" 
	    		                value = {description}
	    		                onChange={e => setDescription(e.target.value)}
	    		                required
	    	                />
	    	            </Form.Group>

	    	            <Form.Group controlId="price" className="mb-3">
	    	                <Form.Label>Product Price</Form.Label>
	    	                <Form.Control 
	    		                type="number" 
	    		                placeholder="Enter Product Price" 
	    		                value = {price}
	    		                onChange={e => setPrice(e.target.value)}
	    		                required
	    	                />
	    	            </Form.Group>
										<Form.Group controlId="price" className="mb-3">
	    	                <Form.Label>Category</Form.Label>
	    	                <Form.Control 
	    		                type="text" 
	    		                placeholder="Enter Category type" 
	    		                value = {category}
	    		                onChange={e => setCategory(e.target.value)}
	    		                required
	    	                />
	    	            </Form.Group>

	    	            <Form.Group controlId="stocks" className="mb-3">
	    	                <Form.Label>Product Stocks</Form.Label>
	    	                <Form.Control 
	    		                type="number" 
	    		                placeholder="Enter Product Stocks" 
	    		                value = {stocks}
	    		                onChange={e => setStocks(e.target.value)}
	    		                required
	    	                />
	    	            </Form.Group>
	    			</Modal.Body>

	    			<Modal.Footer>
	    				{ isActive 
	    					? 
	    					<Button variant="primary" type="submit" id="submitBtn">
	    						Save
	    					</Button>
	    				    : 
	    				    <Button variant="danger" type="submit" id="submitBtn" disabled>
	    				    	Save
	    				    </Button>
	    				}
	    				<Button variant="secondary" onClick={closeAdd}>
	    					Close
	    				</Button>
	    			</Modal.Footer>

	    		</Form>	
	    	</Modal>
	    {/*End of modal for adding product*/}

    	{/*Modal for Editing a product*/}
			<Modal show={showEdit} fullscreen={true} onHide={closeEdit}>
    		<Form onSubmit={e => editProduct(e)}>

    			<Modal.Header closeButton>
    				<Modal.Title>Edit a Product</Modal.Title>
    			</Modal.Header>

    			<Modal.Body>
    	        	<Form.Group controlId="name" className="mb-3">
    	                <Form.Label>Product Name</Form.Label>
    	                <Form.Control 
    		                type="text" 
    		                placeholder="Enter Product Name" 
    		                value = {name}
    		                onChange={e => setName(e.target.value)}
    		                required
    	                />
    	            </Form.Group>

    	            <Form.Group controlId="description" className="mb-3">
    	                <Form.Label>Product Description</Form.Label>
    	                <Form.Control
    	                	as="textarea"
    	                	rows={3}
    		                placeholder="Enter Product Description" 
    		                value = {description}
    		                onChange={e => setDescription(e.target.value)}
    		                required
    	                />
    	            </Form.Group>

    	            <Form.Group controlId="price" className="mb-3">
    	                <Form.Label>Product Price</Form.Label>
    	                <Form.Control 
    		                type="number" 
    		                placeholder="Enter Product Price" 
    		                value = {price}
    		                onChange={e => setPrice(e.target.value)}
    		                required
    	                />
    	            </Form.Group>

									<Form.Group controlId="price" className="mb-3">
	    	                <Form.Label>Category</Form.Label>
	    	                <Form.Control 
	    		                type="text" 
	    		                placeholder="Enter Category type" 
	    		                value = {category}
	    		                onChange={e => setCategory(e.target.value)}
	    		                required
	    	                />
	    	            </Form.Group>

    	            <Form.Group controlId="stocks" className="mb-3">
    	                <Form.Label>Product Stocks</Form.Label>
    	                <Form.Control 
    		                type="number" 
    		                placeholder="Enter Product Stocks" 
    		                value = {stocks}
    		                onChange={e => setStocks(e.target.value)}
    		                required
    	                />
    	            </Form.Group>
    			</Modal.Body>

    			<Modal.Footer>
    				{ isActive 
    					? 
    					<Button variant="primary" type="submit" id="submitBtn">
    						Save
    					</Button>
    				    : 
    				    <Button variant="danger" type="submit" id="submitBtn" disabled>
    				    	Save
    				    </Button>
    				}
    				<Button variant="secondary" onClick={closeEdit}>
    					Close
    				</Button>
    			</Modal.Footer>

    		</Form>	
    	</Modal>
    	{/*End of modal for editing a product*/}
			
			
		</>
		:
		<Navigate to="/products" />
	)
}
